---
title: "Kurze Beschreibung"
date: 2019-12-17T15:33:08+01:00
draft: false
subtitle: ""
image: ""
tags: []
---

Die Welt gliedert sich in das unmittelbare Geschehen und das, was sich in der
Anschauung erst zu einem ganzen fügen kann.

Hier schreibe ich aus dem Bereich, wo sich für mich das Geschehen der Gegenwart
zu einem Ganzen zusammenfügt.
