---
title: "Warum immer gleich Steinzeit"
date: 2021-03-24T10:00:57+01:00
draft: false
subtitle: ""
image: ""
tags: [bildung]
---

Sobald ich die moderne Welt und die herrschende Rolle der Maschinisierung des Lebens als nicht unbedingt wohltuend benenne,
schlägt mir die Frage entgegen, ob ich denn in die Steinzeit zurückwolle.

Das ist natürlich völliger Blödsinn.

Liegt doch bei dieser Frage die Anschauung zugrunde, es hätte nur einen möglichen Weg der Entwicklung gegeben.

Die zahlreichen Abzweigungen der geschichtlichen Entwicklung werden hartnäckig ignoriert und drücken das Ausmaß der Unbildung
des jeweiligen Gesprächspartners aus. Diese Unbildung mutmaßt, es gäbe zur weiteren Maschinisierung des Lebens keine Alternative
und jedes Annehmen einer Begrenzung, jeder Punkt an dem man sich besinnt und fragt, ob das Tun gut sei, führe zu einem primitiven
und rohen Leben, daß sich nur mit der Steinzeit vergleichen lasse.

Wäre es nicht schön, wenn diese Leute etwas mehr Bildung genossen hätten, um ihre Hybris zumindest in ihrer Bedeutung erfassen zu
können.
