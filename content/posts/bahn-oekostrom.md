---
title: "Bahn Oekostrom"
date: 2019-09-17T02:15:22+01:00
draft: false
tags: [gesellschaft]
---

Letztens bin ich auf einen [Artikel](https://www.dw.com/de/wie-grün-ist-die-deutsche-bahn-wirklich/a-41894678)
gestoßen, der erklärt, wie die Deutsche Bahn es schafft, daß die
Fernzüge mit 100% Ökostrom betrieben
werden können. Wobei die Deutsche Bahn genau das nicht tut, sondern
einen recht originellen Weg geht, indem der gesamte Strombedarf aus
verschiedenen Quellen zusammengeworfen wird und dann buchhalterisch
so umgerechnet wird, daß die Fernzüge ausschließlich mit Strom aus
erneuerbaren Quellen betrieben werden. Es ist also keineswegs so,
daß die Deutsche Bahn ihre Fernzüge mit Strom aus erneuerbaren
Energiequellen betreibt, und es ist sogar möglich, daß sie auch
gar nicht in der Lage wäre, dies zu tun, da ein Stromnetz sicherlich
eine gewisse Menge an Strom braucht, um die Spannung über das
gesamte Netz aufrechterhalten zu können. Aber nun gut, ich bin
kein Experte für Stromnetze und so kann ich nur feststellen, daß aus
logischer Sicht die Deutsche Bahn ihre Fernzüge zu 100% mit Strom aus
erneuerbaren Energiequellen betriebe, wenn sie Strom aus nicht
erneuerbaren Energiequellen nicht einkaufte und dann tatsächlich alle
Fernzüge fahren könnten.

Lassen wir jedoch mal diese eher nüchterne Betrachtung und schauen
uns an, wie es sich verhielte, wenn wir in unserem Leben eine
ähnliche Haltung zur Wirklichkeit an den Tag legten.

In meiner fünfköpfigen Familie ist es so, daß jeder
seine ganz eigenen Bedürfnisse und Vorstellungen von einem guten und
geeignetem Leben entwickelt hat. Mittlerweise ist es so, daß
wir Milch aus unterschiedlichen Quellen bevorzugen. Die Frau mag
die Milch aus Glasflaschen, ein Kind mag gerne Bioheumilch, das andere
Soyamilch und der Rest bevorzugt Hafermilch. Wenn ich nun der ganzen
verschiedenen Gefäße überdrüssig bin und aus Gründen der Platzersparnis
die Reste in eine Flasche zusammenschütte, kann ich beim Ausschenken der
Milch beim gemeinsamen Frühstück für jedes Kind feststellen, daß
es jeweils 100% seiner bevorzugten Milch bekomme, da in der Mischung
ein genügend großer Anteil des jeweiligen Getränks vorhanden war.

Nun ist das sicherlich nicht das, was die einzelnen Familienmitglieder
erwartet haben, und dennoch habe ich kalkulatorisch vollkommen recht,
es wäre für jeden genügend seiner bevorzugten Milch in der Flasche
gewesen, aber keiner hat das bekommen, was er wollte.

Auch wenn dieses Beispiel wie alle anderen Vergleiche hinken mag,
veranschaulicht es die Lüge der Unternehmens Deutsche Bahn und die
mit der Wirklichkeit nicht übereinstimmenden Vorstellungen, welche
dieses Unternehmen dem bereitwilligen Konsumenten zur Entscheidungsfindung
erzählt, wobei fairerweise nicht festgestellt sein möchte, daß es
sich damit von anderen Unternehmen des industriellen Kapitalismus unterscheidet,
sondern in diese Falle zu einem schönen anschaulichen Vergleich eingeladen hat.
