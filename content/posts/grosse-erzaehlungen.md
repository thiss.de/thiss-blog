---
title: "Große Erzählungen"
subtitle: ""
date: 2021-03-20T07:40:48+01:00
image: ""
draft: false
tags: [gesellschaft]
---

Im Zuge der sogenannten Coronakrise kommen auch die neuen Propheten hervor,
die die Krise als Strafe Gottes erklären wollen. Als Buße für die Hybris des
Zeitgeistes.

Von derartigen Erzählungen halte ich nicht viel. Meiner Auffassung nach, sind die
großen Erzählungen bereits erzählt. Als Kind seiner Zeit sehe ich nur die Dummheit
am agieren. Denn Dumm ist es, wenn man unterscheidet ohne das Wesen der Dinge zu
bezeichnen, sondern sich in allem täuscht, was man sieht und benennt.
