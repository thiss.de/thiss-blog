---
title: "Der Begriff Der Bildung"
date: 2019-12-17T15:33:08+01:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, bildung]
---

Mir scheint, daß unter dem Begriff der Bildung mittlerweile verstanden
werde, eine Tätigkeit oder einen Vorgang aufgrund von Ideen auszuüben,
die nicht in der Gegenwart präsent sind.

Aber vom Bild, vom Zusammenfügen unterschiedlicher Prinzipien zu einem
Ganzen, zu einem Bild, redet niemand mehr, obwohl doch das gerade Bildung
ausmacht: die Fähgikeit die Vielzahl von Erscheinungen der einströmenden
Wirklichkeit zusammenzufassen und zu einer Einheit zu ordnen.
