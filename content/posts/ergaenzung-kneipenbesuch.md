---
title: "Ergänzung Kneipenbesuch"
date: 2021-03-20T08:04:31+01:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, philosophie, erkenntnistheorie]
---

Vor einiger Zeit habe ich von einem [Kneipenbesuch]({{< relref "ein-kneipenbesuch" >}}) erzählt.

Nun hat mich Ludwik Fleck mit einer kleinen Untersuchung zur Entstehung und Entwicklung wissenschaftlicher Tatsachen
darauf hingewiesen, daß die Auffassung dieser jungen Dame gar nicht so verkehrt war.

Meine naive Vorstellung, daß etwas Erkanntes das Wesen der Welt beschreibt, somit als möglicher Gegenstand der Betrachtung
für jeden Menschen offenliegt, entspricht zwar dem ursprünglichen Wahrheitsbegriff, womit ein Satz wahr ist, wenn er
mit der Welt übereinstimmt.

Diese Vorstellung entspricht aber nicht dem Wahrheitsbegriff innerhalb der Wissenschaften. Dort zählt tatsächlich
die Übereinstimmung der Gemeinschaft des sozialen Systems der Wissenschaftler, welches nicht einmal mehr das Bemühen
erkennen läßt, die Wesentlichkeit der Welt zu erfassen und zu benennen.

Vielmehr fokussieren sich die Teilnehmer dieses sozialen Systems der Wissenschaft darauf, gewisse
Modelle zu bestätigen und diese Abbilder Welt für die Wirklichkeit zu setzen.

Das perfide an diesem Vorgehen ist der Ausschluß der Möglichkeit, eine Erkenntnis außerhalb dieses Systems zu erhalten.
Die Unzulänglichkeiten des wissenschaftlichen Erkennens, werden pauschauliert auf alle anderen Erkenntnismöglichkeiten
übertragen. Kurz gesagt: "Was die Wissenschaft nicht erkennen kann, kann niemand erkennen".

Wir können nur froh sein, daß Menschen wie Ludwik Fleck und Paul Feyerabend auf diese Unzulänglichkeiten des
wissenschaftlichen Erkennens hingewiesen haben, wir also nicht naiv den Verkündungen der wissenschaftlichen
Propheten blind vertrauen müssen.
