---
title: "Wissenschafts Gläubigkeit"
date: 2021-03-26T10:17:43+01:00
draft: false
subtitle: ""
image: ""
tags: [bildung, erkenntnistheorie]
---

Auf [stern.de](https://www.stern.de/lifestyle/pandemie-peter-lohmeyer-kontert-corona-kritik-mit-fakten-30452324.html) war folgendes zu lesen:

> Schauspieler Peter Lohmeyer (59, „Das Wunder von Bern“) hat im Gespräch mit Maskenverweigern und Anhängern von Verschwörungstheorien meist eine klare Linie. „Meine Strategie ist in der Pandemie, den Glauben an die Wissenschaft weiterzugeben“, sagte er der Deutschen Presse-Agentur in Hamburg.

Damit hat der besagte Schauspieler treffend beschrieben, um was es beim Umgang mit wissenschaftlichen Aussagen geht: sie müssen
geglaubt werden. Es handelt sich eben nicht um Wahrheit oder um allgemeingültige Sätze, die für sich gelten, sondern um
Glauben, die rein subjektive Annahme der Zutreffendheit von Aussagen.

Es wäre zu wünschen, daß es mehr Äußerungen gäbe, die sich so selbst enttarnen, wie diese.

Einige Literaturempfehlungen:
  - A.F. Chalmers, Wege der Wissenschaft
  - Ludwik Fleck, Entstehung und Entwicklung wissenschaftlicher Tatsachen
  - Paul Feyerabend, Wider den Methodenzwang
