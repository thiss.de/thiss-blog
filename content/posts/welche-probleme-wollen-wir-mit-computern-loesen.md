---
title: "Welche Probleme wollen wir mit Computern lösen"
date: 2019-12-17T10:50:45+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Es gibt Aufgaben, die sich immer wieder mit der gleichen Abfolge an
Schritten lösen lassen. Das sind genau die Aufgaben, die ein Computer
bestens bewältigen kann.
Alles was reproduzierbar und in identischer Abfolge immer die gleiche Form
des Ablaufs hat, ist eine Computer lösbare Aufgabe.

Dafür haben Informatiker sehr viel elaboriertere Termini entwickelt, aber
anschaulich ist es genau das.

Jetzt stellt sich mir die Frage, welche Bereiche des Lebens man dieser
Reproduizierbarkeit unterwerfen will.
Denn alles was ein Computerprogramm ausgeben kann, ist Ergebnis einer
genauen Abfolge von einzelnen Anweisungen.
Da helfen auch Neuronale Netzwerke nichts, die werden vorher so trainiert
das sie genau eine Aufgabe lösen können. Theoretisch wäre es möglich das
trainierte Modell auch von Hand zu erstellen, es dürfte nur einiges an
Aufwand bedeuten.
