---
title: "Expertentum Aus Faulheit"
date: 2021-03-27T07:14:18+01:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, softwareentwicklung]
---

Gesellschaften neigen irgendwann dazu, ein jegliches Problem durch funktionale Differenzierung lösen zu wollen.

Das führt dazu, daß ein neu auftretendes Problem erst einmal nicht versucht wird zu lösen, sondern stattdessen nach einem
Experten gesucht wird, von dem man glaubt, er könne das Problem lösen.

Allerdings führt die fehlende Bereitschaft, das Problem in seiner Struktur und seinen Zusammenhängen zu verstehen dazu, daß
die letztlich erbrachte Lösung hinsichtlich ihrer Qualität und Geeignetheit gar nicht beurteilt werden kann.

Die Forderung, die ein auftretendes Problem stellt, lautet: begreife ersteinmal die Situation, untersuche die Zusammenhänge in
denen es auftaucht, finde die Quelle des Problems, trinke in Ruhe einen Kaffee.
Erst dann begebe dich auf die Suche nach Hilfe, wenn du an dieser Stelle nicht weiterkommst.

Die vorauseilende Suche nach Experten hat meiner Erfahrung nach noch nie zu einer schnelleren oder belastbareren Lösung eines
Problems geführt, als das eigene Bemühen.
