---
title: "Big Data Systeme vs Relationale Systeme"
date: 2019-12-17T08:34:32+01:00
draft: false
subtitle: ""
image: ""
tags: [software-probleme]
---

Wer sein Computer Denken mit Datenbanken begonnen hat, wird einige Hürden nehmen
müssen, bevor er begreifen kann, wie ein Big-Data System funktioniert.

Der offensichtliche Zusammenhang zwischen Flexibilität einer Datentransformation und
der dafür notwendigen Verarbeitungsdauer erschließt sich intuitiv und ist
sicherlich auch mathematisch beweisbar.

Ich weiß jetzt nicht, ob irgendjemand das schon als Gesetz formuliert hat, so in der
Art, daß ein System nur in Hinsicht auf Datenmenge, Geschwindigkeit, Flexibilität
optimiert werden kann, mir wäre das jetzt zu langweilig, das auszuarbeiten, ich löse
das Problem lieber direkt, indem ich diese Einschränkung annehme und meine Systeme so
plane, daß sie damit umgehen können.

Deshalb ist die Beschreibung der zu erwartenden Transformationsschritte so relevant
für die Planung eines Datensystems, nur daraus ergibt sich in welchem Format die
Daten gespeichert werden müssen, respektive ob die Daten mehrfach in unterschiedlichen
Formate vorgehalten werden müssen, um die Anforderungen zu erfüllen.

Es ist für mich erschreckend, daß dieser Zusammenhang noch von keinem Softwarearchitekten,
mit dem ich in meiner beruflichen Laufbahn zusammengearbeitet habe, berücksichtigt wurde,
bzw. daß ihnen dieser Zusammenhang noch nichtmals bekannt war.
