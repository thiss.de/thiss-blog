---
title: "Nicht Denkende Querdenker"
date: 2021-03-28T09:58:42+02:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, politik]
---

Letztens habe ich bei einer der sogenannten "Querdenker" Demonstrationen vorbeigeschaut.

Da gab es dann den Ruf "Söder muß weg" und alternativ auch "Merkel muß weg".

Wenn ich solche Sprüche höre, frage ich mich schon, inwiefern hier tatsächlich gedacht wird.

Denn es ist doch offensichtlich, daß es nicht an diesen Personen liegt, daß diese Personen nicht die Ursache von
politischen Mißständen sind.

Schließlich leben wir nicht mehr in einer Monarchie, in der der König oder die Königin alleinige Verantwortung für die
Geschicke des Landes haben.

In einem komplexen System sind sämtliche Entscheidungen systemisch bedingt. Es kann also nicht zielführend sein, einzelne
Personen auszutauschen, da ihre Entscheidungskompetenz überhaupt nicht ausreicht, an den wesentlichen Bedingungen des
gemeinschaftlichen Miteinanders etwas zu ändern.

Wir dürfen nämlich nicht vergessen, daß der Maßstab für ein Gesellschaftssystem, im Bewußtsein jedes Einzelnen für die
gemeinschaftlichen Belange besteht.

In der Konsequenz ergibt sich daraus ein Weg aus Mißständen durch die Bewußtseinsbildung bei der Gemeinschaft.

Diese ganzen Zusammenhänge von sozialen Systemen hat Niklas Luhmann in "Systemtheorie der Gesellschaft" zwar auf seine sehr
eigene, aber nicht weniger zutreffende Weise beschrieben.

Von jemanden, der sich als Denker benennt, egal ob "Quer-", "Klar-" oder ohne Suffix, erwarte ich die Bereitschaft, sich
wenigstens grundlegend mit den wesentlichen Begriffen eines Anschaaungsbereiches befaßt zu haben, bevor er zu diesem
seine eigenen Aussagen beisteuert.
