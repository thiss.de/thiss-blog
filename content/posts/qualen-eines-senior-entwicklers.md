---
title: "Qualen eines Big-Data Entwicklers"
date: 2021-03-28T20:53:15+02:00
draft: false
subtitle: ""
image: ""
tags: [softwareentwicklung]
---

Eine der größeren Qualen in meiner Zeit als Senior Entwickler für Big-Data-Anwendungen war die Tatsache,
daß ich den gesamten Big-Data Stack von der Picke auf gelernt hatte und dann in Umgebungen entwickeln mußte,
die von Software-Architekten designt wurden, die nicht im Ansatz verstanden haben, mit welchem Technologiestack sie
eigentlich gerade arbeiten.

Bei der täglichen Arbeit stößt man dann ständig auf unausgegorene, ungeeignete, unverstandene Lösungen, die einem
den Hals schwellen lassen.

Deswegen mußte ich nach einiger Zeit, aus gesundheitlichen Gründen diese Arbeit aufgeben.
Als Profi kann man nur mit Profis arbeiten.
Was sich aber so in der deutschen Industrie bewegt ist allerdings mit Konzepten wie Professionalität oder Sachkunde nicht zu beschreiben.
