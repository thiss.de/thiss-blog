---
title: "Strategie"
date: 2019-12-26T05:40:38+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Wie kann die Idee in der gegebenen Struktur umgesetzt werden?
Welche strukturellen Möglichkeiten können genutzt, welche müssen geändert werden?
Welches Vorgehen ergibt sich aus der Idee und der Struktur, ist es eine Revolution oder eine Evolution?
Sollen und können Merkmale, vorhandene Strukturen einfach ausgewechselt werden oder gilt es eine gewachsene Struktur in eine neue Richtung zu lenken?
Gibt es in der Struktur die Möglichkeit diese Idee wachsen zu lassen, oder wird eine völlig neue Struktur benötigt? Wenn ja, wie kann die strukturelle Änderung gefördet werden? Wenn nein, wie kann die Idee in der Struktur zur Entwicklung gebracht werden?