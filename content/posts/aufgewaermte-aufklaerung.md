---
title: "Aufgewärmte Aufklärung"
date: 2021-04-05T08:36:02+02:00
draft: false
subtitle: ""
image: ""
tags: [philosophie]
---


Wenn mir heutzutage etwas ganz besonders auf die Nerven geht, dann sind das die Versuche, die Aufklärung wieder Aufzuwärmen.

Dann wird ganz edel davon gesprochen, der Mensch müsse sich aus seiner Unmündigkeit befreien und den Mut zur eigenen Erkenntnis haben.

Vergessen wird dabei, daß genau dies niemals das Ziel der Aufklärung war. Es ging vielmehr darum die subjektive Fertigkeit des
Verarbeitens von Sinneseindrücken zum Maßstab der Wahrheit zu erklären.
Methodisch sollten Sinneswahrnehmungen in ein System eingeordnet werden, damit die Angst vor der dem Unbekannten, vor der
Veränderung der Welt nicht die Tatkraft hindert.

Denn Grundlage des Vorgehens war die wissenschaftliche Methodik der Newton'schen Mechanik in Verbindung mit Ockhams Rasiermesser.

Diese erschien den Philosophen der Aufklärung so einleuchtend, daß sie diese Art der Weltbeschreibung allen anderen Konzepten für
überlegen hielten. Romano Guardini hat das treffend beschrieben als eine Verlagerung der Wahrheit eines Satzes über die Welt in
die subjektive Verarbeitungsfähigkeit. Ein Satz ist wahr, weil er einem Menschen wahr scheint
(Romano Guardini, "Natur und Schöpfung" in "Welt und Person", S. 19, Würzburg 2018).

Der schlimmste Aufklärer war Immanuel Kant. Denn sein Ziel war direkt die Unterwerfung des einzelnen Menschen unter die soziale
Gemeinschaft. Diese Auffassung verkennt, daß eine soziale Gemeinschaft sich nur aus eigenständigen Personen begründen kann, die
aufgrund von Einsicht gemeinsamer Interessen einen Zusammenschluß bildet. Mit einer freiheitlichen Ordnung ist die Kant'sche
Philosophie nicht zu vereinbaren und die ganzen Apologeten dieser Konzepte vertreten nichts anderes als die totale Entmündigung
des Menschen. Sie geben den einzelnen Menschen frei für den Verbrauch durch die Masse.
