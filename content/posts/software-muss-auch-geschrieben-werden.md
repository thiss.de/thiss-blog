---
title: "Software muss auch geschrieben werden"
date: 2019-12-17T08:25:55+01:00
draft: true
subtitle: ""
image: ""
tags: [software, programmieren, software-probleme]
---

Viele Softwarearchitekten und Projekt Manager scheinen gerne zu vergessen,
daß Software am Ende des Tages auch entwickelt werden muss.

Da vergessen sie dann beim Planen die Möglichkeit Programme zu testen,
in unterschiedlichen Umgebungen lauffähig zu sein, verschiedene neu entwickelte
Features vorführen zu können, also im wesentlichen alles, was dazu führt, 
daß ein Softwareprogramm erfolgreich implementiert werden kann.

