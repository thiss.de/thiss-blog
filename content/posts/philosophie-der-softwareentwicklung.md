---
title: "Philosophie Der Softwareentwicklung"
date: 2019-12-20T06:27:45+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

## Einleitung

Informatik stellt einen Lösungsraum dar, im Umgang des Menschen mit der Welt finden wir einen
Problemraum.

Dem Prinzip nach sollte der Problemraum in den Lösungsraum hinein abgebildet werden, also
für ein Problem eine Lösung gefunden werden.

Es trifft sich aber immer wieder, das eine Lösung vorhanden ist für die ein Problem gesucht wird.

## Grundlagen

Informatik als Fachgebiet löst für sich keine Probleme, sie stellt ein Werkzeug dar,
daß von Menschen zur Lösung von Problmenen eingesetzt werden kann.

Informatik ist dabei Abstraktion über Wirklichkeit.

In der Welt der modernen Softwareentwicklung gibt es zahlreiche Frameworks, Tools, die
als Artefakte der Informatik eine abgeleitete Abstraktion sind.

Der erfolgreiche Einsatz von Software hängt an der Auswahl:

- Welches Framework
- Welche Programmiersprache
- Welche Datenbank
- Welche Betriebsumgebung

Die wichtigste Aufgabe beim Schreiben von Software ist die Auswahl und Ordnung der
Aufgabe:

- Was genau soll das Programm erledigen
- Wie kann ich sicher sein, daß mein Programm das dann auch tut
- Gibt es versteckte Rahmenbedingungen, die noch unklar sind, aber Einfluß auf meinen
  aktuellen Code haben

Sind diese Fragen geklärt geht es an die nächsten Fragen:

- Welche Klassen brauche ich
- Wäre eine DSL sinnvoll
- Wie sollen meine Klassen zur Laufzeit zusammenarbeiten
- Wie fließen die Daten zwischen den Klassen
- Welche Methoden braucht meine Klasse
- Benötigt meine Klassen statische Variablen
- Welche Instanzvariablen benötigt meine Klasse

