---
title: "Kategorien von Softwareproblemen"
date: 2019-12-17T07:27:46+01:00
draft: true
subtitle: ""
image: ""
tags: [programmieren, denken, software-probleme]
---

Wenn eine Aufgabe mit Hilfe von Software gelöst werden soll finden wir 
unterschiedliche Kategorien von Problemen.

- Die Herausforderungen genau zu beschreiben, was die Software tun soll
- Die Möglichkeit, ob Software das überhaupt zu leisten vermag
- Die Umgebungsbedingungen der laufenden Software
- Der Art und Weise wie diese Software dann letztlich benutzt werden soll

Dabei gliedert sich heraus, daß einige Probleme in der Interaktion mit dem Menschen
liegen, der die Software schreibt oder verwendet, andere dem Schreiben von Software
selbst zugeordnet sind.

> Dabei ist zu berücksichtigen, daß Software selber eine Sammlung von Anweisungen ist,
> wie bestimmte Eingangsdaten verändert werden sollen. 
> Also im wesentlichen eine definierte, reproduzierbare Reaktion auf Eingangsdaten.

Informatik als Studienfach an deutschen Universitäten legt die größere Aufmerksamkeit
auf die Probleme des Schreibens von Software. 
Dabei wird zuerst die Abstraktion der Mathematik gesucht, um den angehenden Informatiker
darauf hinzuführen, daß er von der Anschauung des konkret Wahrnehmbaren der Umwelt 
absieht und eine Lösung ohne konkrete Anschauulichkeit entwickeln kann, in einem
System von Abbildern, in dem jedes Element eine ableitbare, nur mittelbare Beziehung
zu anderen Elementen hat.

Letztlich beinhaltet ein Informatiksystem die Möglichkeit der absoluten Perfektion.

Ein Beispiel für ein derartig perfektes Informatik-System ist TeX von Donald E. Knuth,
ein Computerprogramm mit dem sich Texte perfekt und fehlerfrei setzen lassen.
Es zeigt sich aber, daß diese Software dann kaum noch für den Menschen nutzbar ist.
Im Laufe der Zeit wurden dann auch entsprechende Abstraktionen über TeX gesetzt, um
die Arbeit des Textsatzes zu erleichtern und einer größeren Anzahl von Menschen 
zu ermöglichen. Deren bekannteste Abstraktion ist sicherlich LaTeX, weitere sind
Editioren und Entwicklungsumgebungen, die versuchen einen ähnlichen Nutzbarkeit wie
Microsoft Word zu erreichen.
Dabei verliert die Software an ihrer Reinheit und Perfektion durch die von den 
Abstraktionen eingeführten Einschränkungen.

Man könnte also die Frage stellen, ob es mit einem Computer möglich ist, einen
perfekten Büchersatz zu erzeugen und feststellen, daß es mit Hilfe von TeX 
möglich ist.
Hinzu kommt dann, daß es für die meisten Menschen kaum nachvollziehbar ist, 
wie der Vorgang des Textsatzes in Software abgebildet wird und deshalb sich
mit einer unzulänglicheren Software befassen.

Irgendwie ist es also ein Konflikt, zwischen einer bestmöglichen Lösung einer
Aufgabe und der Benutzbarkeit dieser Lösung, der aus dem konkreten Einsatz der
Software folgt.

An diesem Punkt setzt die Geeignetheit von anschaulich denkenden Menschen an,
die mit der Lücke zwischen dem Abbilden eines Vorgangs in Software und dem 
Einsatz dieser Abbildung umgehen können und in der Lage sind, verschiedene
Ideen zusammenzuführen. 
Auch wenn das Streben nach einer Einheit, das Denken auf eine Einheit hin in 
unserer Zeit keine Wertschätzung mehr erfährt, ist es für eine Softwareprojekt
unabgänglich, diese Einheit anzustreben, da es schließlich als eine Einheit
funktionieren soll.
