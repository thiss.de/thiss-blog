---
title: "Struktur"
date: 2019-12-26T05:40:32+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Wie verhält sich die Idee mit der Umwelt?
Welche Voraussetzungen verlangt die Idee, damit sie genutzt werden kann?
Welche Bedingungen müssen vorhanden sein, damit es umgesetzt werden kann?
Welche Abhängigkeiten, Voraussetzungen sind für die Entwicklung und Nutzung der Idee nötig?
Wie ist die Interaktion der Idee in einer möglichen Gegenwart?
