---
title: "Idee"
date: 2019-12-26T05:40:25+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Die Form, die in der Gegenwart wirken soll. 
Wie verändert ihr Produkt, ihre Dienstleistung den Umgang des Menschen mit der Welt? 
Welche Entscheidungen nehmen Sie dem Nutzer ihres Produktes ab?
Wie wird das Erleben der Welt durch ihr Produkt verändert?

Berücksichtigen Sie die menschliche Dimension ihrer Idee?